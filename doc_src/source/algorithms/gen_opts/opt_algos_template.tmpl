..
   Copyright 2021 IRT Saint-Exupéry, https://www.irt-saintexupery.com

   This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit
   http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
   Commons, PO Box 1866, Mountain View, CA 94042, USA.

.. _gen_opt_algos:

Optimization algorithms options
===============================

A simple way to solve  :class:`.OptimizationProblem` is to use the API method
:meth:`~gemseo.api.execute_algo`. E.g.

.. code::

    from gemseo.api import execute_algo
    from gemseo.problems.analytical.rosenbrock import Rosenbrock

    problem = Rosenbrock()
    sol = execute_algo(problem, "L-BFGS-B", max_iter=20)
    print sol.f_opt

They are also used in all MDO and DOE scenarios in the dictionary
passed to the :meth:`.MDODiscipline.execute` method.

List of available algorithms :
{% for algo in opt_algos%}:ref:`{{algo}}_options` -
{% endfor %}

{% for algo in opt_algos%}
.. _{{algo}}_options:

{{algo}}
{{ (algo|length)*'-' }}

Description
~~~~~~~~~~~

{{opt_descriptions[algo]}}

{% if opt_url[algo] is not none %}
{% set temp = '`External link <' + opt_url[algo] + '>`__' %}
{{temp}}
{{ (temp|length)*'~' }}
{%endif %}

Options
~~~~~~~

{% for option in opt_options[algo]|dictsort %}
- **{{option[0]}}**, :code:`{{option[1]['ptype']}}` - {{option[1]['description']}}

{% endfor %}
{% endfor %}
