..
   Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com

   This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit
   http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
   Commons, PO Box 1866, Mountain View, CA 94042, USA.

.. uml::

	class MixtureOfExperts {
	 +ABBR
	 +LABELS
	 #LOCAL_INPUT
	 #LOCAL_OUTPUT
	 +regress_algo
	 +classif_algo
	 +cluster_algo
	 +regress_params
	 +classif_params
	 +cluster_params
	 +labels
	 +n_clusters
	 +set_clusterer()
	 +set_classifier()
	 +set_regressor()
	 +predict_class()
	 +predict_local_model()
	 +__str__()
	 #fit()
	 #fit_clusters()
	 #fit_classifier()
	 #fit_regressors()
	 #predict_all()
	 #predict()
	 #predict_jacobian()
	 #predict_jacobian_hard()
	 #predict_jacobian_soft()
	 #save_algo()
	 +load_algo()
	 #get_objects_to_save()
	}

	abstract class MLRegressionAlgo {
	 +learn()
	 #predict()
	 #predict_jacobian()
	}

	abstract class MLClassificationAlgo {
	 +learn()
	 #predict()
	 #predict_proba()
	}

	abstract class MLClusteringAlgo {
	 +n_clusters
	 +labels
	 +learn()
	}

	MLRegressionAlgo <|-down- MixtureOfExperts
	MixtureOfExperts *-- RegressionModelFactory
	MixtureOfExperts *-- ClassificationModelFactory
	MixtureOfExperts *-- ClusteringModelFactory
	RegressionModelFactory "1" --> "n" MLRegressionAlgo
	ClassificationModelFactory --> MLClassificationAlgo
	ClusteringModelFactory --> MLClusteringAlgo
